//
//  JSONParameterEncoder.swift
//  NetworkLayer
//
//  Created by Ananth Chepuri on 30/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

public struct JSONParameterEncoder: ParameterEncoder {
    
    static func endode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        
        do {
            let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            urlRequest.httpBody = jsonAsData
            
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        } catch {
            throw NetworkError.encodingFailed
        }
    }
}
