//
//  ParameterEncoding.swift
//  NetworkLayer
//
//  Created by Ananth Chepuri on 11/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

public typealias Parameters = [String:Any]

public enum NetworkError : String, Error {
    case perametersNil = "Parameters were nil."
    case encodingFailed = "Parameter encoding failed."
    case missingURL = "URL is nil."
}

protocol ParameterEncoder {
    static func endode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}
