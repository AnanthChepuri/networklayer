//
//  HTTPMethod.swift
//  NetworkLayer
//
//  Created by Ananth Chepuri on 11/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

public enum HTTPMethod : String {
    case get    = "GET"
    case post   = "POST"
    case put    = "PUT"
    case patch  = "PATCH"
    case delete = "DELETE"
}
