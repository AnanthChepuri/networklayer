//
//  EndPointType.swift
//  NetworkLayer
//
//  Created by Ananth Chepuri on 11/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders { get }
}
