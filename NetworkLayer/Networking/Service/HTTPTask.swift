//
//  HTTPTask.swift
//  NetworkLayer
//
//  Created by Ananth Chepuri on 11/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String:String]

public enum HTTPTask {
    
    case request
    case requestPerameters(bodyParameters: Parameters?, urlParameters: Parameters?)
    case requestPerametersAndHeaders(bodyParameters: Parameters?, urlParameters: Parameters?, additionHeaders: HTTPHeaders)
}
