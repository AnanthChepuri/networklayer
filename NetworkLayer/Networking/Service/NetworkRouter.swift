//
//  NetworkRouter.swift
//  NetworkLayer
//
//  Created by Ananth Chepuri on 30/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ()

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func request(_ route:EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}
